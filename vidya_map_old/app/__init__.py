from flask import Flask, render_template, url_for
from flask_socketio import SocketIO, emit
from flask_scss import Scss

app = Flask(__name__)
Scss(app)
app.debug = True
app.config['SECRET_KEY'] = '\x81\xde\x89\x87\x16\x87G\x1a\xb36s\xfa\x97l0}F\x1el\x7fW-F?'

socketio = SocketIO(app, static_dir="static", asset_dir="assets")

if __name__ == "__main__":
    socketio.run(app, debug=True)

number = 0
markers = {'1': {'style': {'background': '#efbf14', 'border_color': '#b88d2c'},'pos': {'top': 0, 'left': 0}},
    '2': {'style': {'background': '#efbf14', 'border_color': '#b88d2c'},'pos': {'top': 100, 'left': 200}}}

@app.route("/")
@app.route("/index")
@app.route("/interact")
def interact():
    #print('stuff - ' + str(markers['2']['pos']['left'])
    return render_template("interact.htm") #TODO: Zustand der Seite übertragen

@socketio.on("connect")
def handle_connect():
    for key in markers:
        response = markers[key].copy();
        response["element"] = key;
        emit("marker_update", response)

@socketio.on("request_reload")
def handle_request_reload():
    for key in markers:
        response = markers[key].copy();
        response["element"] = key;
        emit("marker_update", response)

@socketio.on("marker_move")
def handle_marker_move(data):
    global number
    global marker

    number += 1
    # print("position_move - n:" + str(number) + " element:" + data["element"] + " left:" + str(data["left"]) + " top:" + str(data["top"]))
    markers[data['element']]['pos']['top'] = data['top']
    markers[data['element']]['pos']['left'] = data['left']
    print(str(markers))

    socketio.emit("marker_move", data)
