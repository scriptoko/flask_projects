$("#slider").slider();

$(document).ready(function(e) {

  $("img").on("mousemove", function(e) {
    e.preventDefault();
    $("#output").html("");
    $("#output").append("map-offset-left: " + $("#map").offset().left + "<br />");
    $("#output").append("map-offset-left: " + $("#map").offset().top + "<br />");
    $("#output").append("e.pageX: " + e.pageX + "<br />");
    $("#output").append("e.pageY: " + e.pageY + "<br />");
    $("#output").append("------------------------------" + "<br />");
    $("#output").append("marker-1-s.css(\"left\"): " + $("#marker-1-s").css("left") + "<br />");
    $("#output").append("marker-1-s.css(\"top\"): " + $("#marker-1-s").css("top") + "<br />");
    $("#output").append("------------------------------" + "<br />");
    $("#output").append("marker-2-s.css(\"left\"): " + $("#marker-2-s").css("left") + "<br />");
    $("#output").append("marker-2-s.css(\"top\"): " + $("#marker-2-s").css("top") + "<br />");
  });

  var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port);

  socket.on("marker_move", function(data) {
    var marker = $("#marker-" + data.element);
    var shadowMarker = $("#marker-" + data.element + "-s");

    shadowMarker.css({'left': data.left, 'top': data.top});
    marker.stop();
    marker.animate({'left': data.left, 'top': data.top}, 3000);
  });

  socket.on("marker_update", function(data) {
    var marker = $("#marker-" + data.element);
    var shadowMarker = $("#marker-" + data.element + "-s");

    shadowMarker.css({'left': data.pos.left, 'top': data.pos.top, 'background': data.style.background, 'border-color': data.style.border_color});
    marker.css({'left': data.pos.left, 'top': data.pos.top, 'background': data.style.background, 'border-color': data.style.border_color});
  });


  $(".marker").on('mousedown', function(e){
    var marker = $(this);
    var shadowMarker = $("#" + $(this).attr("id") + "-s");
    shadowMarker.css({'left': parseInt(marker.css("left")), 'top': parseInt(marker.css("top"))});

    var initialX = e.pageX - $("#map").offset().left + $("#map").scrollLeft()- parseInt(shadowMarker.css("left"));
    var initialY = e.pageY - $("#map").offset().top + $("#map").scrollTop() - parseInt(shadowMarker.css("top"));

    $(window).on('mousemove', function(e) {
      var posX = e.pageX - $("#map").offset().left + $("#map").scrollLeft() - initialX;
      var posY = e.pageY - $("#map").offset().top + $("#map").scrollTop() - initialY;
      shadowMarker.css({'left': Math.round(posX), 'top': Math.round(posY)});
    });

    $(window).on('mouseup', function(e) {
      send_marker_move(socket, marker.attr('id'), parseInt(shadowMarker.css("left")), parseInt(shadowMarker.css("top")));
      $(this).off('mousemove');
      $(this).off('mouseup');
    });
  });

  $(".marker-shadow").on('mousedown', function(e) {
    var shadowMarker = $(this);
    var marker =  $("#" + $(this).attr("id").slice(0, -"-s".length));

    var initialX = e.pageX - $("#map").offset().left + $("#map").scrollLeft()- parseInt(shadowMarker.css("left"));
    var initialY = e.pageY - $("#map").offset().top + $("#map").scrollTop() - parseInt(shadowMarker.css("top"));

    $(window).on('mousemove', function(e) {
      var posX = e.pageX - $("#map").offset().left + $("#map").scrollLeft() - initialX;
      var posY = e.pageY - $("#map").offset().top + $("#map").scrollTop() - initialY;
      shadowMarker.css({'left': Math.round(posX), 'top': Math.round(posY)});
    });

    $(window).on('mouseup', function(e) {
      send_marker_move(socket, marker.attr('id'), parseInt(shadowMarker.css("left")), parseInt(shadowMarker.css("top")));
      $(this).off('mousemove');
      $(this).off('mouseup');
    });
  });
});

function send_marker_move(socket, id, posLeft, posTop) {
  socket.emit("marker_move", {'element': id.substring(id.lastIndexOf('-') + 1), 'left': Math.round(posLeft), 'top': Math.round(posTop)});
};
