from flask import render_template
from app import app

@app.route('/')
@app.route('/index')
def index():
    user = {'username': 'Miguel'}
    posts = [
        {
            'author': {'username': 'John'},
            'body': 'Beautiful day in Portland!'
        },
        {
            'author': {'username': 'Susan'},
            'body': 'The Avengers movie was so cool!'
        },
        {
            'author': {'username': 'essem'},
            'body': 'Ich weiß worum es in dieser Folge geht.'
        }
    ]
    return render_template('index.htm', title='Home', user=user, posts=posts)
