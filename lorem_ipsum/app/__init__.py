from flask import Flask
from flask_socketio import SocketIO, send, emit

app = Flask(__name__)
app.config['SECRET_KEY'] = 'it_symbolizes_my_inner_torment';
socketio = SocketIO(app)

loremFile = open("lorem.txt", "r")
loremText = loremFile.read()
loremFile.close()
stuff = 0

@socketio.on('connect')
def handle_message():
    print('client connected')
    global stuff

    while(len(loremText) > stuff):
        socketio.emit("loremChar", loremText[stuff])
        socketio.sleep(0.1)
        stuff += 1

if __name__ == '__main__':
    socketio.run(app)

from app import routes
