from flask import Flask, render_template, url_for

app = Flask(__name__)
app.config["SECRET_KEY"] = "PLACEHOLDER_KEY"

if __name__ == "__main__":
    app.run()

@app.route("/")
@app.route("/index")
def index():
    return render_template("index.htm")
