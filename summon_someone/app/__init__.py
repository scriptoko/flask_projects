from flask import Flask, render_template
from flask_socketio import SocketIO

app = Flask(__name__)
app.config['SECRET_KEY'] = 'O\xa3\x94\x15\xafP\x0f\xe3\x9f\x19\xc5\xc3\x92#\xc8\xc3\x94X\xf2\x82\xfb\xa6\xa0\x9d'
app.config['TEMPLATES_AUTO_RELOAD'] = True
socketio = SocketIO(app, debug=True, use_reloader=True)

if __name__ == "__main__":
    socketio.run(app)

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.htm')
